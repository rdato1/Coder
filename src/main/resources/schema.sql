DROP TABLE cliente IF EXISTS;
CREATE TABLE cliente (
   dni VARCHAR(255) NOT NULL,
   nombre VARCHAR(255),
   apellido VARCHAR(255),
   fechanacimiento TIMESTAMP,
   CONSTRAINT pk_cliente PRIMARY KEY (dni)
);

DROP TABLE venta IF EXISTS;
CREATE TABLE venta (
  id INT AUTO_INCREMENT NOT NULL,
   preciofinal FLOAT NOT NULL,
   fechacompra TIMESTAMP,
   dnicliente VARCHAR(255),
   CONSTRAINT pk_venta PRIMARY KEY (id)
);
ALTER TABLE venta ADD CONSTRAINT FK_VENTA_ON_CLIENTE_DNI FOREIGN KEY (dnicliente) REFERENCES cliente (dni);

DROP TABLE lineaventa IF EXISTS;
CREATE TABLE lineaventa (
  id INT AUTO_INCREMENT NOT NULL,
   idventa INT NOT NULL,
   idproducto INT NOT NULL,
   cantidad INT NOT NULL,
   precio float not null,
   CONSTRAINT pk_lineaventa PRIMARY KEY (id)
);
ALTER TABLE lineaventa ADD CONSTRAINT FK_LINEAVENTA_ON_VENTA FOREIGN KEY (idventa) REFERENCES venta (id);

DROP TABLE producto IF EXISTS;
CREATE TABLE producto (
  id INT NOT NULL,
   descripcion VARCHAR(255),
   precio FLOAT NOT NULL,
   marca VARCHAR(255),
   stock int not null,
   CONSTRAINT pk_producto PRIMARY KEY (id)
);