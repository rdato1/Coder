insert into cliente (dni, nombre, apellido, fechanacimiento)
values
('12345','Gonzalo','Gomez','1994-05-10'),
('54321','Abel','Arido','1997-05-10')
;

insert into producto (id, descripcion, precio, marca, stock)
values
(1,'Tomate',10.5,'Coto',5),
(2,'Pera',11,'Vea',10),
(3,'Manzana',5,'Disco',15);

insert into venta (dnicliente, preciofinal,fechacompra)
values
('12345', 10.5,'2022-07-18'),
('54321', 30,'2022-07-21');

insert into lineaventa (idventa, idproducto, cantidad, precio)
values
(1,1,2,20),
(1,2,3,22),
(1,3,5,25),
(2,2,1,10.5),
(2,3,3,15);