package com.example.ProyFinal.EntregaFinal.errorHandler;

public class ApiException extends Exception {
    private String message;
    public ApiException(String message) {
        super(message);
    }
}
