package com.example.ProyFinal.EntregaFinal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Entrega2v1DatoRocioApplication {

	public static void main(String[] args) {
		SpringApplication.run(Entrega2v1DatoRocioApplication.class, args);
	}

}
