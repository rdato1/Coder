package com.example.ProyFinal.EntregaFinal.service;



import com.example.ProyFinal.EntregaFinal.DTO.LineaVentaResponse;
import com.example.ProyFinal.EntregaFinal.entity.LineaVenta;

import java.util.List;

public interface LineaVentaService {
    List<LineaVentaResponse> buscarTodos();

    LineaVentaResponse buscarPorId(int id);

    LineaVentaResponse actualizar(LineaVenta lineaVenta);

    LineaVentaResponse crear(LineaVenta lineaVenta);
}
