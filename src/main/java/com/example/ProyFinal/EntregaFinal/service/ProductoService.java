package com.example.ProyFinal.EntregaFinal.service;

import com.example.ProyFinal.EntregaFinal.DTO.ProductoResponse;
import com.example.ProyFinal.EntregaFinal.entity.Producto;

import java.util.List;

public interface ProductoService {
    List<ProductoResponse> buscarTodos();

    ProductoResponse buscarPorId(int id);

    ProductoResponse actualizar(Producto producto);

    ProductoResponse crear(Producto producto);
}
