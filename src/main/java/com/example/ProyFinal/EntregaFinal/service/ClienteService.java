package com.example.ProyFinal.EntregaFinal.service;

import com.example.ProyFinal.EntregaFinal.DTO.ClienteResponse;
import com.example.ProyFinal.EntregaFinal.entity.Cliente;

import java.util.List;


public interface ClienteService {
    List<ClienteResponse> buscarTodos();

    ClienteResponse buscarPorDni(String dni);

    ClienteResponse actualizar(Cliente cliente);

    ClienteResponse crear(Cliente cliente);
}
