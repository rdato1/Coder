package com.example.ProyFinal.EntregaFinal.service;

import com.example.ProyFinal.EntregaFinal.entity.Venta;
import com.example.ProyFinal.EntregaFinal.DTO.VentaResponse;
import com.example.ProyFinal.EntregaFinal.DTO.VentaPost;
import com.example.ProyFinal.EntregaFinal.errorHandler.ApiException;

import java.util.List;

public interface VentaService {
    List<VentaResponse> buscarTodos();

    List<VentaResponse> buscarPorDni(String dni);

    VentaResponse actualizar(Venta venta);

    VentaResponse crear(VentaPost ventaPost) throws ApiException;
}
