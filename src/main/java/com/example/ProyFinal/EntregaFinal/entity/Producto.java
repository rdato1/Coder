package com.example.ProyFinal.EntregaFinal.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "Producto")
public class Producto {
    @Id
    @Column(name = "ID")
    int id;

    @Column(name = "DESCRIPCION")
    String descripción;

    @Column(name = "PRECIO")
    float precio;

    @Column(name = "MARCA")
    String marca;

    @Column(name = "STOCK")
    int stock;

    @OneToMany(mappedBy = "producto", cascade = CascadeType.ALL)
    private List<LineaVenta> lineaVenta;

    @Override
    public String toString() {
        return "Producto{" +
                "id=" + id +
                ", descripción='" + descripción + '\'' +
                ", precio=" + precio +
                ", marca='" + marca + '\'' +
                ", stock=" + stock +
                '}';
    }
}
