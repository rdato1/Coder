package com.example.ProyFinal.EntregaFinal.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "LINEAVENTA")
public class LineaVenta {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    int id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDVENTA")
    private Venta venta;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "IDPRODUCTO")
    private Producto producto;

    @Column(name = "CANTIDAD")
    int cantidad;

    @Column(name = "PRECIO")
    float precio;

    public LineaVenta(Venta venta, Producto producto, int cantidad, float precio) {
        this.venta = venta;
        this.producto = producto;
        this.cantidad = cantidad;
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "LineaVenta{" +
                "id=" + id +
                ", cantidad=" + cantidad +
                ", precio=" + precio +
                '}';
    }
}
