package com.example.ProyFinal.EntregaFinal.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Data
@Entity
@Table(name = "VENTA")
@NoArgsConstructor
@AllArgsConstructor
public class Venta {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "ID")
    int id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DNICLIENTE")
    private Cliente cliente;

    @Column(name = "PRECIOFINAL")
    float precioFinal;

    @Column(name = "FECHACOMPRA")
    LocalDate fechaCompra;

    @OneToMany(mappedBy = "venta", cascade = CascadeType.ALL)
    private List<LineaVenta> lineaVenta;


    public Venta(Cliente cliente, LocalDate fechaCompra) {
        this.cliente = cliente;
        this.precioFinal = 0;
        this.fechaCompra = fechaCompra;
    }

}
