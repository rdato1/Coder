package com.example.ProyFinal.EntregaFinal.controller;

import com.example.ProyFinal.EntregaFinal.DTO.LineaVentaResponse;
import com.example.ProyFinal.EntregaFinal.entity.LineaVenta;
import com.example.ProyFinal.EntregaFinal.service.LineaVentaService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/lineaVenta")
public class LineaVentaController {

    private final LineaVentaService lineaVentaService;

    @GetMapping("")
    public List<LineaVentaResponse> obtenerLineaVenta() {
        return lineaVentaService.buscarTodos();
    }

    @GetMapping("/id/{dni}")
    public LineaVentaResponse buscarLineaVentaPorId(@PathVariable int id) {
        return lineaVentaService.buscarPorId(id);
    }

    @PostMapping("/actualizar")
    public LineaVentaResponse actualizarLineaVenta(@RequestBody LineaVenta lineaVenta) {
        return lineaVentaService.actualizar(lineaVenta);
    }

    @PostMapping("/crear")
    public LineaVentaResponse crearLineaVenta(@RequestBody LineaVenta lineaVenta) {
        return lineaVentaService.crear(lineaVenta);
    }
}
