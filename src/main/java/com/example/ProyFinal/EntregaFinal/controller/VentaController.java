package com.example.ProyFinal.EntregaFinal.controller;

import com.example.ProyFinal.EntregaFinal.DTO.VentaPost;
import com.example.ProyFinal.EntregaFinal.DTO.VentaResponse;
import com.example.ProyFinal.EntregaFinal.entity.Venta;
import com.example.ProyFinal.EntregaFinal.service.VentaService;
import com.example.ProyFinal.EntregaFinal.errorHandler.ApiException;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/venta")
public class VentaController {
    private final VentaService ventaService;

    @GetMapping("")
    public List<VentaResponse> obtenerVentas() {
        return ventaService.buscarTodos();
    }

    @GetMapping("/dni/{dni}")
    public List<VentaResponse> buscarVentasPorDni(@PathVariable String dni) {
        return ventaService.buscarPorDni(dni);
    }

    @PostMapping("/actualizar")
    public VentaResponse actualizarVenta(@RequestBody Venta venta) {
        return ventaService.actualizar(venta);
    }

    @PostMapping("/crear")
    public VentaResponse crearVenta(@RequestBody VentaPost ventaPost) throws ApiException {
             return ventaService.crear(ventaPost);
    }
}
