package com.example.ProyFinal.EntregaFinal.controller;

import com.example.ProyFinal.EntregaFinal.entity.Cliente;
import com.example.ProyFinal.EntregaFinal.DTO.ClienteResponse;
import com.example.ProyFinal.EntregaFinal.service.ClienteService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping ("/cliente")
public class ClienteController {
    private final ClienteService clienteService;

    @GetMapping("")
    public List<ClienteResponse> obtenerClientes() {
        return clienteService.buscarTodos();
    }

    @GetMapping("/dni/{dni}")
    public ClienteResponse buscarClientePorDni(@PathVariable String dni) {
        return clienteService.buscarPorDni(dni);
    }

    @PostMapping("/actualizar")
    public ClienteResponse actualizarCliente(@RequestBody Cliente cliente) {
        return clienteService.actualizar(cliente);
    }

    @PostMapping("/crear")
    public ClienteResponse crearCliente(@RequestBody Cliente cliente) {
        return clienteService.crear(cliente);
    }
}
