package com.example.ProyFinal.EntregaFinal.controller;

import com.example.ProyFinal.EntregaFinal.DTO.ProductoResponse;
import com.example.ProyFinal.EntregaFinal.entity.Producto;
import com.example.ProyFinal.EntregaFinal.service.ProductoService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/producto")
public class ProductoController {

    private final ProductoService productoService;

    @GetMapping("")
    public List<ProductoResponse> obtenerProducto() {
        return productoService.buscarTodos();
    }

    @GetMapping("/id/{dni}")
    public ProductoResponse buscarProductoPorId(@PathVariable int id) {
        return productoService.buscarPorId(id);
    }

    @PostMapping("/actualizar")
    public ProductoResponse actualizarProducto(@RequestBody Producto producto) {
        return productoService.actualizar(producto);
    }

    @PostMapping("/crear")
    public ProductoResponse crearProducto(@RequestBody Producto producto) {
        return productoService.crear(producto);
    }

}
