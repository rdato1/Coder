package com.example.ProyFinal.EntregaFinal.DTO;

import com.example.ProyFinal.EntregaFinal.DTO.ProductoToVenta;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class LineaToVenta implements Serializable {
    private final ProductoToVenta producto;
    private final int cantidad;
}
