package com.example.ProyFinal.EntregaFinal.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;

@Data
@AllArgsConstructor
public class ClienteToVenta implements Serializable {
    private final String dni;
}
