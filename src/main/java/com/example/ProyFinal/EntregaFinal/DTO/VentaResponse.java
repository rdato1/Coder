package com.example.ProyFinal.EntregaFinal.DTO;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

@Data
public class VentaResponse implements Serializable {
    private final String dniCliente;
    private final float precioFinal;
    private final LocalDate fechaCompra;
}
