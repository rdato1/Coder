package com.example.ProyFinal.EntregaFinal.DTO;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProductoToVenta implements Serializable {
    private final int id;
}
