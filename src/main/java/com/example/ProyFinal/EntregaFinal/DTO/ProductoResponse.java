package com.example.ProyFinal.EntregaFinal.DTO;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProductoResponse implements Serializable {
    private final int id;
    private final String descripción;
    private final float precio;
    private final String marca;
    private final int stock;
}
