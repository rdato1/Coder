package com.example.ProyFinal.EntregaFinal.DTO;

import lombok.Data;

import java.io.Serializable;

@Data
public class LineaVentaResponse implements Serializable {
    private final int id;
    private final int cantidad;
    private final float precio;
}
