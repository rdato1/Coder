package com.example.ProyFinal.EntregaFinal.DTO;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class VentaPost implements Serializable {
    private final int id;
    private final ClienteToVenta cliente;
    private final List<LineaToVenta> lineas;
}
