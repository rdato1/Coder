package com.example.ProyFinal.EntregaFinal.repository;

import com.example.ProyFinal.EntregaFinal.entity.Cliente;
import com.example.ProyFinal.EntregaFinal.entity.Venta;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VentaRepository extends JpaRepository<Venta, Integer> {

    List<Venta> findAllByCliente(Cliente cliente);
}