package com.example.ProyFinal.EntregaFinal.repository;

import com.example.ProyFinal.EntregaFinal.entity.LineaVenta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LineaVentaRepository extends JpaRepository<LineaVenta, Integer> {
}