package com.example.ProyFinal.EntregaFinal.repository;

import com.example.ProyFinal.EntregaFinal.entity.Producto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductoRepository extends JpaRepository<Producto, Integer> {
}