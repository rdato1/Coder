package com.example.ProyFinal.EntregaFinal.serviceImpl;

import com.example.ProyFinal.EntregaFinal.DTO.LineaToVenta;
import com.example.ProyFinal.EntregaFinal.entity.*;
import com.example.ProyFinal.EntregaFinal.DTO.VentaResponse;
import com.example.ProyFinal.EntregaFinal.DTO.VentaPost;
import com.example.ProyFinal.EntregaFinal.errorHandler.ApiException;
import com.example.ProyFinal.EntregaFinal.repository.ClienteRepository;
import com.example.ProyFinal.EntregaFinal.repository.LineaVentaRepository;
import com.example.ProyFinal.EntregaFinal.repository.ProductoRepository;
import com.example.ProyFinal.EntregaFinal.repository.VentaRepository;
import com.example.ProyFinal.EntregaFinal.service.VentaService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class VentaServiceImpl implements VentaService {

    private final VentaRepository ventaRepository;
    private final ClienteRepository clienteRepository;
    private final ProductoRepository productoRepository;
    private final LineaVentaRepository lineaVentaRepository;

    @Override
    public List<VentaResponse> buscarTodos() {
        List<Venta> posts = ventaRepository.findAll();
        return posts.stream()
                .map(this::ventaToResponse)
                .collect(Collectors.toList());
    }

    @Override
    public List<VentaResponse> buscarPorDni(String dni) {
        Cliente cliente = clienteRepository.findById(dni).orElse(null);
        List<Venta> posts = ventaRepository.findAllByCliente(cliente);
        return posts.stream()
                .map(this::ventaToResponse)
                .collect(Collectors.toList());
    }

    @Override
    public VentaResponse actualizar(Venta venta) {
        return ventaToResponse(ventaRepository.save(venta));
    }

    @Override
    public VentaResponse crear(VentaPost ventaPost) throws ApiException {

        boolean existeCliente = existeCliente(ventaPost.getCliente().getDni());
        boolean existenProductos = existenProductosYStock(ventaPost.getLineas()) ;

        if (!existeCliente) {
            throw new ApiException("No existe el cliente");
        }
        if (!existenProductos){
            throw new ApiException("No hay disponibilidad de productos");
        }

        if (existeCliente && existenProductos){
            RestTemplate restTemplate = new RestTemplate();
            RelojAPI relojAPI = restTemplate.getForObject("http://worldclockapi.com/api/json/utc/now", RelojAPI.class);
            LocalDate fechaCompra = LocalDate.now();
            String fechaCompraString = relojAPI.getCurrentDateTime();
            try {
                fechaCompra = new SimpleDateFormat("yyyy-MM-dd'T'mm:ss'Z'").parse(fechaCompraString).toInstant()
                        .atZone(ZoneId.systemDefault())
                        .toLocalDate();
            } catch (ParseException e) {
            }


            Venta venta = new Venta(
                    dniToCliente(ventaPost.getCliente().getDni()),
                    fechaCompra);
            ventaRepository.save(venta);

            for (int i = 0; i<ventaPost.getLineas().size();i++) {
                Producto producto = productoRepository.findById(ventaPost.getLineas().get(i).getProducto().getId()).orElse(null);
                int cantidad = ventaPost.getLineas().get(i).getCantidad();
                LineaVenta lineaVenta = new LineaVenta(
                        venta,
                        producto,
                        cantidad,
                        calculatePrecioLinea(producto,cantidad));
                lineaVentaRepository.save(lineaVenta);
                calculatePrecioFinal(venta,lineaVenta);

                producto.setStock(producto.getStock() - cantidad);
                productoRepository.save(producto);
            }


            return ventaToResponse(venta);
        }
        return ventaToResponse(null);
    }

    private boolean existenProductosYStock(List<LineaToVenta> lineas) {
        for (LineaToVenta lineaToVenta : lineas){
            if (!productoRepository.existsById(lineaToVenta.getProducto().getId())){
                return false;
            } else {
                Producto producto = productoRepository.findById(lineaToVenta.getProducto().getId()).orElse(null);
                int stock = producto.getStock();
                if (stock < lineaToVenta.getCantidad()){
                    return false;
                }
            }
        }
        return true;
    }

    private boolean existeCliente(String dni) {
        return clienteRepository.existsById(dni);
    }

    private void calculatePrecioFinal(Venta venta, LineaVenta lineaVenta) {
        venta.setPrecioFinal(venta.getPrecioFinal()+lineaVenta.getPrecio());
    }

    private float calculatePrecioLinea(Producto producto, int cantidad) {
        return cantidad * producto.getPrecio();
    }

    private VentaResponse ventaToResponse(Venta venta){
        return new VentaResponse(venta.getCliente().getDni(), venta.getPrecioFinal(),venta.getFechaCompra());
    }
    private Cliente dniToCliente(String dni){
        return clienteRepository.findById(dni).orElse(null);
    }


}
