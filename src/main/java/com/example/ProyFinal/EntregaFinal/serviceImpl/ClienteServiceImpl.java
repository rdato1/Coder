package com.example.ProyFinal.EntregaFinal.serviceImpl;

import com.example.ProyFinal.EntregaFinal.DTO.ClienteResponse;
import com.example.ProyFinal.EntregaFinal.entity.Cliente;
import com.example.ProyFinal.EntregaFinal.repository.ClienteRepository;
import com.example.ProyFinal.EntregaFinal.service.ClienteService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ClienteServiceImpl implements ClienteService {

    private final ClienteRepository clienteRepository;

    @Override
    public List<ClienteResponse> buscarTodos() {
        List<Cliente> posts = clienteRepository.findAll();
        return posts.stream()
                .map(this::clienteToResponse)
                .collect(Collectors.toList());
    }

    @Override
    public ClienteResponse buscarPorDni(String dni) {
        return clienteToResponse(clienteRepository.findById(dni).orElse(null));
    }


    @Override
    public ClienteResponse actualizar(Cliente cliente) {
        return clienteToResponse(clienteRepository.save(cliente));
    }

    @Override
    public ClienteResponse crear(Cliente cliente) {
        return clienteToResponse(clienteRepository.save(cliente));
    }

    private ClienteResponse clienteToResponse(Cliente cliente){
        int edad = Period.between(cliente.getFechaNacimiento().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()
                ,LocalDate.now())
                .getYears();
        return new ClienteResponse(cliente.getDni(), cliente.getNombre(), cliente.getApellido(),edad);
    }
}
