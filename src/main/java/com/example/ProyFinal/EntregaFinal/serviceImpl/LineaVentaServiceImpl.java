package com.example.ProyFinal.EntregaFinal.serviceImpl;

import com.example.ProyFinal.EntregaFinal.DTO.LineaVentaResponse;
import com.example.ProyFinal.EntregaFinal.entity.LineaVenta;
import com.example.ProyFinal.EntregaFinal.repository.LineaVentaRepository;
import com.example.ProyFinal.EntregaFinal.service.LineaVentaService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class LineaVentaServiceImpl implements LineaVentaService {

    private final LineaVentaRepository lineaVentaRepository;


    @Override
    public List<LineaVentaResponse> buscarTodos() {
        List<LineaVenta> posts = lineaVentaRepository.findAll();
        return posts.stream()
                .map(this::lineaVentaToResponse)
                .collect(Collectors.toList());
    }

    @Override
    public LineaVentaResponse buscarPorId(int id) {
        return lineaVentaToResponse(lineaVentaRepository.findById(id).orElse(null));
    }

    @Override
    public LineaVentaResponse actualizar(LineaVenta lineaVenta) {
        return lineaVentaToResponse(lineaVentaRepository.save(lineaVenta));
    }

    @Override
    public LineaVentaResponse crear(LineaVenta lineaVenta) {
        return lineaVentaToResponse(lineaVentaRepository.save(lineaVenta));
    }

    private LineaVentaResponse lineaVentaToResponse(LineaVenta lineaVenta){
        return new LineaVentaResponse(lineaVenta.getId(), lineaVenta.getCantidad(), lineaVenta.getPrecio());
    }
}
