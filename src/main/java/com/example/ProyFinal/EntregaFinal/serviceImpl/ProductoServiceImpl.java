package com.example.ProyFinal.EntregaFinal.serviceImpl;

import com.example.ProyFinal.EntregaFinal.DTO.ProductoResponse;
import com.example.ProyFinal.EntregaFinal.entity.Producto;
import com.example.ProyFinal.EntregaFinal.repository.ProductoRepository;
import com.example.ProyFinal.EntregaFinal.service.ProductoService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductoServiceImpl implements ProductoService {

    private final ProductoRepository productoRepository;
    @Override
    public List<ProductoResponse> buscarTodos() {
        List<Producto> posts = productoRepository.findAll();
        return posts.stream()
                .map(this::productoToResponse)
                .collect(Collectors.toList());
    }

    @Override
    public ProductoResponse buscarPorId(int id) {
        return productoToResponse(productoRepository.findById(id).orElse(null));
    }

    @Override
    public ProductoResponse actualizar(Producto producto) {
        return productoToResponse(productoRepository.save(producto));
    }

    @Override
    public ProductoResponse crear(Producto producto) {
        return productoToResponse(productoRepository.save(producto));
    }

    private ProductoResponse productoToResponse(Producto producto){
        return new ProductoResponse(producto.getId(), producto.getDescripción(), producto.getPrecio(), producto.getMarca(), producto.getStock());
    }
}
